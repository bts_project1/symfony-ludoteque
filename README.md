# Symfony - LudoTeque
1er projet avec symfony - 2024 Pôle BTS Alternance

## Info de connexion & bdd
- Un export de la base de donnée se trouve dans le dossier assets/ressources
- user: user@test.fr | test1234
- admin: admin@test.fr | test1234

## Setup project
- git clone
- composer install
- symfony server:start