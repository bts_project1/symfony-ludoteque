-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 25 avr. 2024 à 15:27
-- Version du serveur : 8.0.30
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ludoteque`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Action'),
(2, 'Adventure'),
(3, 'RPG'),
(4, 'Strategy'),
(5, 'Sport'),
(6, 'Racing'),
(7, 'Simulation'),
(8, 'Survival'),
(9, 'Horror'),
(10, 'Puzzle'),
(11, 'Sandbox'),
(12, 'Shooter');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20240418071253', '2024-04-18 07:13:04', 321);

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

CREATE TABLE `game` (
  `id` int NOT NULL,
  `publisher_id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `releas_date` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `pegi` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `game`
--

INSERT INTO `game` (`id`, `publisher_id`, `title`, `releas_date`, `pegi`) VALUES
(1, 2, 'Assassin\'s Creed Valhalla', '2020-11-10 00:00:00', 18),
(2, 11, 'Cyberpunk 2077', '2020-12-10 00:00:00', 18),
(3, 1, 'FIFA 22', '2021-10-01 00:00:00', 3),
(4, 6, 'The Last of Us Part II', '2020-06-19 00:00:00', 18),
(5, 4, 'Animal Crossing: New Horizons', '2020-03-20 00:00:00', 3),
(6, 3, 'Call of Duty: Warzone', '2020-03-10 00:00:00', 18),
(7, 4, 'Mario Kart 8 Deluxe', '2017-04-28 00:00:00', 3),
(8, 10, 'Resident Evil Village', '2021-05-07 00:00:00', 18),
(9, 6, 'Death Stranding', '2019-11-08 00:00:00', 18),
(10, 7, 'Halo Infinite', '2021-12-08 00:00:00', 16),
(11, 4, 'Super Mario Odyssey', '2017-10-27 00:00:00', 7),
(12, 4, 'The Legend of Zelda: Breath of the Wild', '2017-03-03 00:00:00', 12),
(13, 8, 'Final Fantasy VII Remake', '2020-04-10 00:00:00', 16),
(14, 5, 'Grand Theft Auto V', '2013-09-17 00:00:00', 18),
(15, 5, 'Red Dead Redemption 2', '2018-10-26 00:00:00', 18),
(16, 7, 'Minecraft', '2011-11-18 00:00:00', 7),
(17, 7, 'Forza Horizon 5', '2021-11-09 00:00:00', 3),
(18, 2, 'The Crew', '2014-12-02 00:00:00', 12),
(19, 1, 'Need for Speed: Unbound', '2022-11-29 00:00:00', 16),
(20, 12, 'Farming Simulator 22', '2021-11-22 00:00:00', 3),
(21, 7, 'Flight Simulator', '2020-08-18 00:00:00', 3),
(22, 13, 'Euro Truck Simulator 2', '2012-10-19 00:00:00', 3),
(23, 2, 'Rainbow Six Siege', '2015-12-01 00:00:00', 18),
(24, 6, 'Helldivers 2', '2024-02-08 00:00:00', 18);

-- --------------------------------------------------------

--
-- Structure de la table `game_category`
--

CREATE TABLE `game_category` (
  `game_id` int NOT NULL,
  `category_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `game_category`
--

INSERT INTO `game_category` (`game_id`, `category_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(2, 3),
(3, 5),
(4, 1),
(4, 9),
(5, 2),
(5, 7),
(5, 11),
(6, 1),
(6, 12),
(7, 6),
(8, 1),
(8, 2),
(8, 9),
(9, 2),
(10, 12),
(11, 2),
(12, 2),
(12, 10),
(13, 3),
(14, 1),
(14, 2),
(14, 6),
(15, 1),
(16, 11),
(17, 6),
(18, 6),
(19, 6),
(20, 7),
(21, 7),
(22, 7),
(23, 1),
(23, 4),
(23, 12),
(24, 1),
(24, 2),
(24, 12);

-- --------------------------------------------------------

--
-- Structure de la table `publisher`
--

CREATE TABLE `publisher` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `publisher`
--

INSERT INTO `publisher` (`id`, `name`, `country`) VALUES
(1, 'Electronic Arts', 'USA'),
(2, 'Ubisoft', 'France'),
(3, 'Activision Blizzard', 'USA'),
(4, 'Nintendo', 'Japan'),
(5, 'Rockstar Games', 'USA'),
(6, 'Sony Interactive Entertainment', 'Japan'),
(7, 'Microsoft Studios', 'USA'),
(8, 'Square Enix', 'Japan'),
(9, 'Sega', 'Japan'),
(10, 'Capcom', 'Japan'),
(11, 'CD Projekt', 'Poland'),
(12, 'Focus Home Interactive', 'France'),
(13, 'SCS Software', 'Tchéquie');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `game_id` int DEFAULT NULL,
  `reserved_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`) VALUES
(1, 'user@test.fr', '[\"ROLE_ADMIN\"]', '$2y$13$nvkPqK/MCQdDsvyVvInlMefYDk0ECeUgu4lPkcVgZovkkUkSXpaDq');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_232B318C40C86FCE` (`publisher_id`);

--
-- Index pour la table `game_category`
--
ALTER TABLE `game_category`
  ADD PRIMARY KEY (`game_id`,`category_id`),
  ADD KEY `IDX_AD08E6E7E48FD905` (`game_id`),
  ADD KEY `IDX_AD08E6E712469DE2` (`category_id`);

--
-- Index pour la table `publisher`
--
ALTER TABLE `publisher`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_42C84955E48FD905` (`game_id`),
  ADD KEY `IDX_42C84955A76ED395` (`user_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_IDENTIFIER_EMAIL` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `game`
--
ALTER TABLE `game`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `publisher`
--
ALTER TABLE `publisher`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `FK_232B318C40C86FCE` FOREIGN KEY (`publisher_id`) REFERENCES `publisher` (`id`);

--
-- Contraintes pour la table `game_category`
--
ALTER TABLE `game_category`
  ADD CONSTRAINT `FK_AD08E6E712469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AD08E6E7E48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_42C84955A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_42C84955E48FD905` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
