<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Reservation;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ReservationController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {}

    #[Route('/reservation/{id}', name: 'app_send_reservation')]
    public function index(int $id, UserInterface $identifier): Response
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $identifier->getUserIdentifier()]);
        $game = $this->em->getRepository(Game::class)->findOneBy(['id' => $id]);
        $newReservation = new Reservation();
        $newReservation->setUser($user)->setGame($game)->setReservedAt(new DateTimeImmutable());
        $this->em->persist($newReservation);
        $this->em->flush();

        return $this->redirectToRoute('app_home');
    }

    #[Route('profil/return/{id}', name: 'app_return_reservation')]
    public function returnReservation(int $id){
        $reservation = $this->em->getRepository(Reservation::class)->findOneBy(['id' => $id]);
        $reservation->setReturnAt(new DateTimeImmutable());
        $this->em->persist($reservation);
        $this->em->flush();
        return $this->redirectToRoute('app_profil');
    }
}
