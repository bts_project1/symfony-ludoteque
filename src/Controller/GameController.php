<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class GameController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {}
    #[Route('/games', name: 'app_game')]
    public function index(): Response
    {
        $games = $this->em->getRepository(Game::class)->findAll();
        {
            return $this->render('game/index.html.twig', [
                'controller_name' => 'GameController',
                'games' => $games,
            ]);
        }
    }
    #[Route('/game/detail/{id}', name: 'game_detail')]
    public function gameDetail(int $id): Response
    {
        $gameRepository = $this->em->getRepository(Game::class);
        $reservationRepository = $this->em->getRepository(Reservation::class);

        $game = $gameRepository->find($id);
        $reservation = $reservationRepository->findOneBy(['game' => $id, 'returnAt' => null]);
        $isReserved = $reservation !== null;

        return $this->render('game/gameDetail.html.twig', [
            'controller_name' => 'GameController',
            'game' => $game,
            'reserved' => $isReserved,
        ]);
    }

}
