<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Game;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CategoryController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {}

    #[Route('/category/{id}', name: 'app_category')]
    public function index(int $id): Response
    {
        $category = $this->em->getRepository(Category::class)->findOneBy(['id' => $id]);
        $games = $category->getGames();

        return $this->render('category/index.html.twig', [
            'category' => $category,
            'games' => $games,
        ]);
    }
}
