<?php

namespace App\Controller\profil;

use App\Entity\Reservation;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class ProfilController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {}
    #[Route('/profil', name: 'app_profil')]
    public function index(UserInterface $identifier): Response
    {
        $reservations = $this->em->getRepository(Reservation::class)->findBy(['user' => $identifier]);
        $user = $this->em->getRepository(User::class)->findOneBy(['email'=>$identifier->getUserIdentifier()]);
        return $this->render('profil/index.html.twig', [
            'reservations' => $reservations,
            'user' => $user,
        ]);
    }
}
