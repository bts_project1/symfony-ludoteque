<?php

namespace App\Controller;

use App\Entity\Game;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em
    )
    {}

    #[Route('/', name: 'app_home')]
    public function index(): Response{
        $games = $this->em->getRepository(Game::class)->findBy([], ['releasDate' => 'DESC'], 5);
        return $this->render('index.html.twig', [
            'controller_name' => 'HomeController',
            'games' => $games
        ]);
    }

}
