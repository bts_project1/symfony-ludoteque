<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Category;
use App\Entity\Publisher;
use App\Entity\Game;

class AppFixtures extends Fixture
{
//    public function load(ObjectManager $manager): void
//    {
//
//
//        $publishersData = [
//            ['name' => 'Electronic Arts', 'country' => 'USA'],
//            ['name' => 'Ubisoft', 'country' => 'France'],
//            ['name' => 'Activision Blizzard', 'country' => 'USA'],
//            ['name' => 'Nintendo', 'country' => 'Japan'],
//            ['name' => 'Rockstar Games', 'country' => 'USA'],
//            ['name' => 'Sony Interactive Entertainment', 'country' => 'Japan'],
//            ['name' => 'Microsoft Studios', 'country' => 'USA'],
//            ['name' => 'Square Enix', 'country' => 'Japan'],
//            ['name' => 'Sega', 'country' => 'Japan'],
//            ['name' => 'Capcom', 'country' => 'Japan'],
//            ['name' => 'CD Projekt', 'country' => 'Poland'],
//        ];
//
//        foreach ($publishersData as $publisherData) {
//            $publisher = new Publisher();
//            $publisher->setName($publisherData['name']);
//            $publisher->setCountry($publisherData['country']);
//
//            $manager->persist($publisher);
//        }
//        $manager->flush();
//
//
//        // Get some categories
//        $genres = ['Action', 'Adventure', 'RPG', 'Strategy', 'Sport', 'Racing', 'Simulation', 'Survival', 'Horror', 'Puzzle', 'Sandbox', 'Shooter'];
//
//        foreach ($genres as $genreName) {
//            $category = new Category();
//            $category->setName($genreName);
//            $manager->persist($category);
//            $categories[$genreName] = $category;
//        }
//        $manager->flush();
//
//
//        $gamesData = [
//            ['title' => 'Assassin\'s Creed Valhalla', 'release_date' => new \DateTimeImmutable('2020-11-10'), 'genre' => 'Action', 'pegi' => '18', 'publisher' => 'Ubisoft'],
//            ['title' => 'Cyberpunk 2077', 'release_date' => new \DateTimeImmutable('2020-12-10'), 'genre' => 'RPG', 'pegi' => '18', 'publisher' => 'CD Projekt'],
//            ['title' => 'FIFA 22', 'release_date' => new \DateTimeImmutable('2021-10-01'), 'genre' => 'Sport', 'pegi' => '3', 'publisher' => 'Electronic Arts'],
//            ['title' => 'The Last of Us Part II', 'release_date' => new \DateTimeImmutable('2020-06-19'), 'genre' => 'Action', 'pegi' => '18', 'publisher' => 'Sony Interactive Entertainment'],
//            ['title' => 'Animal Crossing: New Horizons', 'release_date' => new \DateTimeImmutable('2020-03-20'), 'genre' => 'Simulation', 'pegi' => '3', 'publisher' => 'Nintendo'],
//            ['title' => 'Call of Duty: Warzone', 'release_date' => new \DateTimeImmutable('2020-03-10'), 'genre' => 'Action', 'pegi' => '18', 'publisher' => 'Activision Blizzard'],
//            ['title' => 'Mario Kart 8 Deluxe', 'release_date' => new \DateTimeImmutable('2017-04-28'), 'genre' => 'Racing', 'pegi' => '3', 'publisher' => 'Nintendo'],
//            ['title' => 'Resident Evil Village', 'release_date' => new \DateTimeImmutable('2021-05-07'), 'genre' => 'Horror', 'pegi' => '18', 'publisher' => 'Capcom'],
//            ['title' => 'Death Stranding', 'release_date' => new \DateTimeImmutable('2019-11-08'), 'genre' => 'Adventure', 'pegi' => '18', 'publisher' => 'Sony Interactive Entertainment'],
//            ['title' => 'Halo Infinite', 'release_date' => new \DateTimeImmutable('2021-12-08'), 'genre' => 'Shooter', 'pegi' => '16', 'publisher' => 'Microsoft Studios'],
//            ['title' => 'Super Mario Odyssey', 'release_date' => new \DateTimeImmutable('2017-10-27'), 'genre' => 'Adventure', 'pegi' => '7', 'publisher' => 'Nintendo'],
//            ['title' => 'The Legend of Zelda: Breath of the Wild', 'release_date' => new \DateTimeImmutable('2017-03-03'), 'genre' => 'Adventure', 'pegi' => '12', 'publisher' => 'Nintendo'],
//            ['title' => 'Final Fantasy VII Remake', 'release_date' => new \DateTimeImmutable('2020-04-10'), 'genre' => 'RPG', 'pegi' => '16', 'publisher' => 'Square Enix'],
//            ['title' => 'Grand Theft Auto V', 'release_date' => new \DateTimeImmutable('2013-09-17'), 'genre' => 'Action', 'pegi' => '18', 'publisher' => 'Rockstar Games'],
//            ['title' => 'Red Dead Redemption 2', 'release_date' => new \DateTimeImmutable('2018-10-26'), 'genre' => 'Action', 'pegi' => '18', 'publisher' => 'Rockstar Games'],
//            ['title' => 'Minecraft', 'release_date' => new \DateTimeImmutable('2011-11-18'), 'genre' => 'Sandbox', 'pegi' => '7', 'publisher' => 'Microsoft Studios']
//        ];
//
//
//        foreach ($gamesData as $gameData) {
//            $game = new Game();
//            $game->setTitle($gameData['title']);
//            $game->setReleasDate($gameData['release_date']);
//            $game->setPegi($gameData['pegi']);
//
//            // Find the publisher object
//            $publisher = $manager->getRepository(Publisher::class)->findOneBy(['name' => $gameData['publisher']]);
//            if ($publisher) {
//                $game->setPublisher($publisher);
//            } else {
//                throw new \Exception('Publisher not found for game: ' . $gameData['title']);
//            }
//
//            // Assign the category based on the genre
//            $genreCategory = $categories[$gameData['genre']];
//            $game->addCategory($genreCategory);
//
//            $manager->persist($game);
//            $manager->flush();
//
//        }
//    }

    public function load(ObjectManager $manager): void
    {
//        $publishersData = [
//            ['name' => 'Electronic Arts', 'country' => 'USA'],
//            ['name' => 'Ubisoft', 'country' => 'France'],
//            ['name' => 'Activision Blizzard', 'country' => 'USA'],
//            ['name' => 'Nintendo', 'country' => 'Japan'],
//            ['name' => 'Rockstar Games', 'country' => 'USA'],
//            ['name' => 'Sony Interactive Entertainment', 'country' => 'Japan'],
//            ['name' => 'Microsoft Studios', 'country' => 'USA'],
//            ['name' => 'Square Enix', 'country' => 'Japan'],
//            ['name' => 'Sega', 'country' => 'Japan'],
//            ['name' => 'Capcom', 'country' => 'Japan'],
//            ['name' => 'CD Projekt', 'country' => 'Poland'],
//        ];
//
//        foreach ($publishersData as $publisherData) {
//            $publisher = new Publisher();
//            $publisher->setName($publisherData['name']);
//            $publisher->setCountry($publisherData['country']);
//
//            $manager->persist($publisher);
//        }
//        $manager->flush();

//        // Get some categories
//        $categoriesData = [
//            ['name' => 'Action'],
//            ['name' => 'Adventure'],
//            ['name' => 'RPG'],
//            ['name' => 'Strategy'],
//            ['name' => 'Sport'],
//            ['name' => 'Racing'],
//            ['name' => 'Simulation'],
//            ['name' => 'Survival'],
//            ['name' => 'Horror'],
//            ['name' => 'Puzzle'],
//            ['name' => 'Sandbox'],
//            ['name' => 'Shooter'],
//        ];
//
//        foreach ($categoriesData as $categoryData) {
//            $category = new Category();
//            $category->setName($categoryData['name']);
//            $manager->persist($category);
//        }
//        $manager->flush();

        $gamesData = [
            // Jeux de course
            ['title' => 'Forza Horizon 5', 'release_date' => new \DateTimeImmutable('2021-11-09'), 'genre' => 'Racing', 'pegi' => '3', 'publisher' => 'Microsoft Studios'],
            ['title' => 'The Crew', 'release_date' => new \DateTimeImmutable('2014-12-02'), 'genre' => 'Racing', 'pegi' => '12', 'publisher' => 'Ubisoft'],
            ['title' => 'Need for Speed: Unbound', 'release_date' => new \DateTimeImmutable('2022-11-29'), 'genre' => 'Racing', 'pegi' => '16', 'publisher' => 'Electronic Arts'],

            // Jeux de simulation
            ['title' => 'Farming Simulator 22', 'release_date' => new \DateTimeImmutable('2021-11-22'), 'genre' => 'Simulation', 'pegi' => '3', 'publisher' => 'Focus Home Interactive'],
            ['title' => 'Flight Simulator', 'release_date' => new \DateTimeImmutable('2020-08-18'), 'genre' => 'Simulation', 'pegi' => '3', 'publisher' => 'Microsoft Studios'],
            ['title' => 'Euro Truck Simulator 2', 'release_date' => new \DateTimeImmutable('2012-10-19'), 'genre' => 'Simulation', 'pegi' => '3', 'publisher' => 'SCS Software'],

            // Autres jeux
            ['title' => 'Rainbow Six Siege', 'release_date' => new \DateTimeImmutable('2015-12-01'), 'genre' => 'Shooter', 'pegi' => '18', 'publisher' => 'Ubisoft'],
            ['title' => 'Helldivers 2', 'release_date' => new \DateTimeImmutable('2024-02-08'), 'genre' => 'Shooter', 'pegi' => '18', 'publisher' => 'Sony Interactive Entertainment'],
        ];

        foreach ($gamesData as $gameData) {
            $game = new Game();
            $game->setTitle($gameData['title']);
            $game->setReleasDate($gameData['release_date']);
            $game->setPegi($gameData['pegi']);

            // Find the publisher object
            $publisher = $manager->getRepository(Publisher::class)->findOneBy(['name' => $gameData['publisher']]);
            if ($publisher) {
                $game->setPublisher($publisher);
            } else {
                throw new \Exception('Publisher not found for game: ' . $gameData['title']);
            }

            // Assign the category based on the genre
            $genreCategory = $manager->getRepository(Category::class)->findOneBy(['name' => $gameData['genre']]);
            if ($genreCategory) {
                $game->addCategory($genreCategory);
            } else {
                throw new \Exception('Category not found for game: ' . $gameData['title']);
            }

            $manager->persist($game);
        }

        $manager->flush();
    }
}
